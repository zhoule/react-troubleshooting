## React Native Trouble

#### How to resolve Error watching file for changes: EMFILE
```
Error watching file for changes: EMFILE
{"code":"EMFILE","errno":"EMFILE","syscall":"Error watching file for changes:","filename":null}
Error: Error watching file for changes: EMFILE
    at exports._errnoException (util.js:1008:11)
    at FSEvent.FSWatcher._handle.onchange (fs.js:1406:11)
```

The following command shows the current limit.

```sh
launchctl limit maxfiles
```

It may show 256 as the lower limit. The following command will change the limits.

```sh
sudo launchctl limit maxfiles 2048 unlimited
```

Next you also want to uninstall `react-native` and reinstall it so you get a version which works better with Sierra. You also want to update Homebrew and install the current version of `watchman`.

```sh
npm uninstall -g react-native-cli
npm install -g react-native-cli
brew update
brew uninstall watchman --force && brew install watchman
```

Now try building and running your React Native project on macOS Sierra.

You will find this may not work for all websites, if that please create the following folder in you computer and it will resolve the problem for you.
```
sudo mkdir -p /usr/local/var/run/watchman/zhoule-state
sudo chown -R zhoule:staff /usr/local/var/run/
```


## Yarn didn't update to latest.

I had the same problem. I (think I) fixed it by doing a bunch of things:

1. Removing yarn binaries manually
    ```
    rm -f /usr/local/bin/yarnpkg
    rm -f /usr/local/bin/yarn
    ```

2. Remove yarn cache:
    ```
    rm -rf /Users/zhoule/.yarn
    ```

3. If you have the following in your .zshrc or .bash_profile, remove it:
    ```
    export PATH="$PATH:`yarn global bin`"
    ```

4. Install via curl or brew
    
    ```
    curl -o- -L https://yarnpkg.com/install.sh | bash
    
    or
    
    brew upgrade yarn
    
    ```

5. Make sure there is the following line in your .zshrc or .bash_profile:
    ```
    export PATH="$HOME/.yarn/bin:$PATH"
    ```
Now if you try to run yarn -v, you should get the latest stable version.


